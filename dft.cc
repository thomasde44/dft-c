#include <stdio.h>
#include <complex>
#include <iostream>
#include <array>
#include <numbers>
#include <math.h>

//static const std::array<int> multiples_2n[] = {0,2,4,8,16,32,64,128,256,512,2048,4096,8192,16384,32768};

template <class T, std::size_t N>
std::ostream& operator<<(std::ostream& o, const std::array<T, N>& arr)
{
    copy(arr.cbegin(), arr.cend(), std::ostream_iterator<T>(o, " \n"));
    return o;
}

template <typename T, std::size_t N>
std::array<T, N> circulant_row_shift(std::array<T, N>& seq){
std::array<T, N> tmp = seq;
    for (int i = 0; i < seq.size(); i++) {
        if (i == 0) {
            seq[i] = tmp[N-1];
        }
        else {
            seq[i] = tmp[i-1];
        }
    }
    return seq;
}

template <typename T, std::size_t N>
std::array<std::array<T, N>, N> make_circulant_matrix(std::array<T, N>& seq) {
    std::array<std::array<T, N>, N> mat;
    mat[0] = seq;
    for (int i = 1; i < (seq.size()); i++) {
        mat[i] = circulant_row_shift(seq);
    }
    return mat;
}

template <typename T, std::size_t N>
std::array<std::array<T, N>, N>
row_multiply_matrix(std::array<T, N>& seq, std::array<std::array<T, N>, N>& mat) {
    for (int i = 0; i < mat[i].size(); i++) {
        for (int j = 0; j < mat[i].size(); j++) {
            mat[i][j] = seq[i]*mat[i][j];
        }
    }
    return mat;
}

template <typename T, std::size_t N>
std::array<T, N> dft(std::array<std::array<T, N>, N>& mat) {
    
    std::array<T, N> result;
    std::complex<float> i(0, 1);
    std::complex<float> img_2pi(0, -2*M_PI);
    
    std::complex<float> k2, j2;
    std::complex<float> sz(N, 0);

    for (int k = 0; k < result.size(); k++) {
        for (int j = 0; j < result.size(); j++) {
            k2 = static_cast<std::complex<float> >(k);
            j2 = static_cast<std::complex<float> >(j);
            result[k] += mat[0][j]*std::exp((img_2pi*k2*j2)/sz);
        }
    }
    return result;
}



int main(){
    
    std::array<std::complex<float>, 64> seq = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63};
    std::array<std::array<std::complex<float>, 64>, 64> matrix;
    
    std::array<std::complex<float>, 8> seq2 = {0,1,2,3,4,5,6,7};
    std::array<std::array<std::complex<float>, 8>, 8> matrix2;
  

//    matrix = make_circulant_matrix(seq);
    matrix2 = make_circulant_matrix(seq2);
    std::cout << dft(matrix2);
    
    
}